PHP5 FPM Nginx image
====================

* Based on `ursus/nginx:latest`
* Uses supervisord
* Configuration (PHP)
	+ `post_max_size` is set to `128M`
	+ `opcache.validate_timestamps` is set to `0`
	+ `date.timezone` is set to `Europe\/Berlin`