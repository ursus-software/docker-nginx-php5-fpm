FROM ursus/nginx:latest
MAINTAINER Djordje Stojanovic <djordje.stojanovic@ursus.rs>
LABEL vendor="Ursus Software"

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -qq -y dpkg-dev php5 php5-fpm php5-cli php5-dev \
	&& apt-get clean -qq

RUN sed -i -e "s/post_max_size = 8M/post_max_size = 128M/g" \
			-e "s/;opcache.validate_timestamps=1/opcache.validate_timestamps=0/g" \
			-e "s/;date.timezone =/date.timezone = Europe\/Berlin/g" /etc/php5/fpm/php.ini \
	&& sed -i -e "s/www-data/root/g" -e "s/;clear_env = no/clear_env = no/g" /etc/php5/fpm/pool.d/www.conf \
	&& sed -i -e "s/DAEMON_ARGS=\"/DAEMON_ARGS=\"--allow-to-run-as-root /g" /etc/init.d/php5-fpm
COPY nginx/default.conf /etc/nginx/sites-available/default

COPY supervisor/conf.d/nginx-php5-fpm.conf /etc/supervisor/conf.d/nginx-php5-fpm.conf

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
